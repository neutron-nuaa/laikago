#!/usr/bin/env python
import json
import sys
import math
from operator import add


class encoder(object):
    def __init__(self):

        ## MORF NO PRIOR ##
        self.set_BC_morf = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_morf = [0.5044372080402942, 0.11179573179088269, 0.22336131133168322, 0.5238323908959595, 0.4897837537401469, 0.23336879209210276, 0.156900132397411, 0.2705401047539295, 0.5045759819857375, 0.4904423579823461, 0.2409376907772342, 0.1710147855545038, 0.2840915832472842, 0.5079500341720962, 0.4593346845127408, 0.21835216964225468, 0.1690032511888188, 0.3258838945822318, 0.5074750373643965, 0.2620683981985637]
        self.set_FT_morf = [-0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04]

        self.set_BC_morf_CPG = [1.0, 0.0]
        self.set_CF_morf_CPG = [1.0, 2.05948852]
        self.set_FT_morf_CPG = [1.0, -0.4]

        ## ALPHA NO PRIOR ##
        self.set_BC_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_FT_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.set_BC_alpha_CPG = [0.5, 0.0]
        self.set_CF_alpha_CPG = [0.5, 0.0]
        self.set_FT_alpha_CPG = [0.5, 0.0]

        ## LAIKAGO NO PRIOR ##
        self.set_BC_laika = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_laika = [0.2236424038363451, 0.10374758225125288, 0.1325345023164375, 0.21309243408110085, 0.2008498013633831, 0.12499944453497111, 0.09701220357872349, 0.1534277494108785, 0.22003872236626223, 0.19559888022361924, 0.11318054969820898, 0.0972276310918886, 0.16178043696769182, 0.22854558692145077, 0.18590051643525377, 0.10340550753498838, 0.0962463730641556, 0.18511283003460186, 0.2623899729123366, 0]
        self.set_FT_laika = [-0.5286093184975102, -0.24522155820562072, -0.31326336931240384, -0.5036730263327652, -0.47473589443597247, -0.2954532327266108, -0.2293015722421638, -0.36264740793092315, -0.5200915259263991, -0.4623246262794827, -0.2675176630945383, -0.22981076454633453, -0.3823901239869558, -0.5401986603424628, -0.43940122094683287, -0.2444130179666586, -0.2274914273883881, -0.4375394167259168, -0.6201944818267482, 0]

        self.set_BC_laika_CPG = [0.0, 0.0]
        self.set_CF_laika_CPG = [1.0, 0.936332]
        self.set_FT_laika_CPG = [0.5, -2.44532925]


    def get_init_parameter_set(self, robot, encoding, numKernals):

        if numKernals == 20:
            if robot == 'MORF':
                set_BC = self.set_BC_morf
                set_CF = self.set_CF_morf
                set_FT = self.set_FT_morf
            elif robot == 'MORF_CPG':
                set_BC = self.set_BC_morf_CPG
                set_CF = self.set_CF_morf_CPG
                set_FT = self.set_FT_morf_CPG
            elif robot == 'ALPHA':
                set_BC = self.set_BC_alpha
                set_CF = self.set_CF_alpha
                set_FT = self.set_CF_alpha
            elif robot == 'ALPHA_CPG':
                set_BC = self.set_BC_alpha_CPG
                set_CF = self.set_CF_alpha_CPG
                set_FT = self.set_FT_alpha_CPG
            elif robot == 'LAIKAGO':
                set_BC = self.set_BC_laika
                set_CF = self.set_CF_laika
                set_FT = self.set_FT_laika
            elif robot == 'LAIKAGO_CPG':
                set_BC = self.set_BC_laika_CPG
                set_CF = self.set_CF_laika_CPG
                set_FT = self.set_FT_laika_CPG
            else:
                print('[ ERROR] Unknown robot')
        elif numKernals == 10:
            if robot == 'MORF':
                set_BC = self.set_BC_morf_10
                set_CF = self.set_CF_morf_10
                set_FT = self.set_FT_morf_10
            elif robot == 'ALPHA':
                set_BC = self.set_BC_alpha_10
                set_CF = self.set_CF_alpha_10
                set_FT = self.set_CF_alpha_10
            elif robot == 'LAIKAGO':
                set_BC = self.set_BC_laika_10
                set_CF = self.set_CF_laika_10
                set_FT = self.set_FT_laika_10
            else:
                print('[ ERROR] Unknown robot')

        if encoding == "indirect":

            #1. chooose this one when use Gaussian noise as initial weights:
            #init_parameter_set = set_BC + set_CF + set_FT
            
            #2.choose this one when use the learned policy from PI^BB as initial weights: 
            #with open('../hypernetwork/hn_training_sample/**.json') as json_file:      
            #     data = json.load(json_file)
            #     init_parameter_set = data['ParameterSet']

            

            #3.choose this one when use hypernetwork's output policy as the policy:
            with open('../hypernetwork/hn_output_policy/006.json') as json_file:      
                 data = json.load(json_file)
                 init_parameter_set = data['ParameterSet']


        elif encoding == "sindirect":
            if robot == 'LAIKAGO':
                init_parameter_set = set_BC + set_BC + set_CF + set_CF + set_FT + set_FT
            else:
                init_parameter_set = set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT
        elif encoding == "direct":
            if robot == 'LAIKAGO':
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT
            else:
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT + set_FT + set_FT
        else:
            print('[ ERROR] Unknown encoding')

        # Sensor Parameter Set
        init_sensor_parameter_set = [0] * (numKernals*9)

        return init_parameter_set, init_sensor_parameter_set
