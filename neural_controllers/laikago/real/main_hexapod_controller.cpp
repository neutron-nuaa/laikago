/*
 * Written by Mathias Thor DEC 30
 * Happy NewYear!
 */

#include "neutronController.h"
#include <csignal>

void signalHandler( int signum ) {
    // system("blinkstick --morph --set-color green --brightness 40");

    // cleanup and close up stuff here
    // terminate program

    exit(signum);
}

int main(int argc,char* argv[])
{
    signal(SIGTERM, signalHandler);
    //  system("blinkstick --morph --set-color red --brightness 40");

    neutronController controller(argc,argv);

    while(controller.runController()){}

    return(0);
}


