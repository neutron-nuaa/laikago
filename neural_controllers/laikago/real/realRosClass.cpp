//
// Created by mat on 8/2/17.
//

#include "realRosClass.h"

realRosClass::realRosClass(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"MORF_controller");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("realROS just started!");

    // Initialize Subscribers
    //motorFeedbackSub=node.subscribe("/joint_states", 1, &realRosClass::motorFeedbackCallback, this);
    joint_IDs=node.subscribe("/morf_hw/joint_IDs", 1, &realRosClass::joint_IDs_CB, this);
    joint_positions=node.subscribe("/morf_hw/joint_positions", 1, &realRosClass::joint_positions_CB, this);
    joint_torques=node.subscribe("/morf_hw/joint_torques", 1, &realRosClass::joint_torques_CB, this);
    joint_velocities=node.subscribe("/morf_hw/joint_velocities", 1, &realRosClass::joint_velocities_CB, this);
    joint_inputVoltage=node.subscribe("/morf_hw/joint_inputVoltage", 1, &realRosClass::joint_inputVoltage_CB, this);
    joint_errorStates=node.subscribe("/morf_hw/joint_errorStates", 1, &realRosClass::joint_errorStates_CB, this);
    joySub=node.subscribe("/joy", 1, &realRosClass::joy_CB, this);
    imu_imu=node.subscribe("/morf_hw/imu", 1, &realRosClass::imu_imu_CB, this);
    imu_euler=node.subscribe("/morf_hw/euler", 1, &realRosClass::imu_euler_CB, this);
    imu_temp=node.subscribe("/morf_hw/temperature", 1, &realRosClass::imu_temp_CB, this);


    // Initialize Publishers
    jointControlPub=node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",1);
    blinkStickPub=node.advertise<std_msgs::ColorRGBA>("/morf_hw/set_all_led",1);

    // Set Rate
    rate = new ros::Rate(60); // 60hz
}

void realRosClass::joint_IDs_CB(const std_msgs::Int32MultiArray& _jointIDs) {
    // TODO: Setup so it is 11 21 31 41 51 61 12 22 32 42 52 62 ect.
    jointIDs = _jointIDs.data;
}

void realRosClass::joint_positions_CB(const std_msgs::Float32MultiArray& _jointPositions) {
    jointPositions = _jointPositions.data;

    if(!shortMorf) {
        jointPositions[CF0] += 1.57079633; // 90 Degree
        jointPositions[CF1] += 1.57079633; // 90 Degree
        jointPositions[CF2] += 1.57079633; // 90 Degree
        jointPositions[CF3] += 1.57079633; // 90 Degree
        jointPositions[CF4] += 1.57079633; // 90 Degree
        jointPositions[CF5] += 1.57079633; // 90 Degree
    }

    jointPositions[BC3] *= -1;
    jointPositions[BC4] *= -1;
    jointPositions[BC5] *= -1;
}

void realRosClass::joint_torques_CB(const std_msgs::Float32MultiArray& _jointTorques) {
    jointTorques = _jointTorques.data;
}

void realRosClass::joint_velocities_CB(const std_msgs::Float32MultiArray& _jointVelocities) {
    jointVelocities = _jointVelocities.data;
}

void realRosClass::joint_errorStates_CB(const std_msgs::Float32MultiArray& _jointErrorStates) {
    jointErrorStates = _jointErrorStates.data;
}

void realRosClass::joint_inputVoltage_CB(const std_msgs::Float32MultiArray& _joint_inputVoltage) {
    jointInputVoltage = _joint_inputVoltage.data;
}

void realRosClass::joy_CB(const sensor_msgs::Joy::ConstPtr& joy){
    axes = joy->axes;
    buttons = joy->buttons;
}

void realRosClass::imu_imu_CB(const sensor_msgs::Imu::ConstPtr &imu) {
    // To be implemented
}

void realRosClass::imu_euler_CB(const geometry_msgs::Vector3::ConstPtr &euler) {
    // To be implemented
}

void realRosClass::imu_temp_CB(const sensor_msgs::Temperature::ConstPtr &temp) {
    // To be implemented
}

void realRosClass::setLegMotorPosition(std::vector<float> positions) {
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();

    if(!shortMorf) {
        positions[CF0] -= 1.57079633; // 90 Degree
        positions[CF1] -= 1.57079633; // 90 Degree
        positions[CF2] -= 1.57079633; // 90 Degree
        positions[CF3] -= 1.57079633; // 90 Degree
        positions[CF4] -= 1.57079633; // 90 Degree
        positions[CF5] -= 1.57079633; // 90 Degree
    }

    positions[BC3] *= -1;
    positions[BC4] *= -1;
    positions[BC5] *= -1;

    std::vector<float> positionsNew = {11,positions.at(BC0),12,positions.at(CF0),13,positions.at(FT0),
                                       21,positions.at(BC1),22,positions.at(CF1),23,positions.at(FT1),
                                       31,positions.at(BC2),32,positions.at(CF2),33,positions.at(FT2),
                                       41,positions.at(BC3),42,positions.at(CF3),43,positions.at(FT3),
                                       51,positions.at(BC4),52,positions.at(CF4),53,positions.at(FT4),
                                       61,positions.at(BC5),62,positions.at(CF5),63,positions.at(FT5)};

    for (float positionsNew : positionsNew) {
        array.data.push_back(positionsNew);
    }

    jointControlPub.publish(array);
}

void realRosClass::setLed(int R, int G, int B, int LED) {
    std_msgs::ColorRGBA color;
    color.r = R;
    color.g = G;
    color.b = B;
    color.a = LED;

    blinkStickPub.publish(color);
}

void realRosClass::rosSpinOnce(){
    ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

realRosClass::~realRosClass() {
    ROS_INFO("realROS just terminated!");
    ros::shutdown();
}

void realRosClass::plot(std::vector<float> data) {
    // PLACE HOLDER
}

void realRosClass::synchronousSimulation(unsigned char) {
    // PLACE HOLDER
}

void realRosClass::triggerSim() {
    // PLACE HOLDER
}

void realRosClass::triggerSimNoWait() {
    // PLACE HOLDER
}


