//
// Created by mat on 12/30/17.
//

#include "neutronController.h"

neutronController::neutronController(int argc,char* argv[]) {
    rosHandle = new realRosClass(argc, argv);
    simulation = false;

    vector<float> weightsMatlab = readParameterSet();
    cout << "[ INFO] Encoding is set to: " << encoding << endl;
    CPGAdaptive = new rbfcpg(weightsMatlab, encoding);

    learner = new dualIntegralLearner(CPGmethod != 3, 20);

    sensorPostprocessor = new postProcessing();
    sensorPostprocessor->setBeta(0.25);

    controllerPostprocessor = new postProcessing();
    controllerPostprocessor->setBeta(0.25);

    sensorAmpPostprocessor = new postProcessing();
    sensorAmpPostprocessor->setBeta(0.04);

    controllerAmpPostprocessor = new postProcessing();
    controllerAmpPostprocessor->setBeta(0.04);

    CPGPeriodPostprocessor = new postProcessing();

    positions.resize(22);

    for (int i = 0; i < 18; ++i) {
        Delayline tmp(tau);
        phaseShift.push_back(tmp);
    }

    //myfile.open ("../V-REP_DATA/log_file.dat");
    //myfile << "time\tcpgPhi\terror\tmaxVel\tmaxTorque\tpositionX\tbodyVel\tangularVelocity\tjointTorque\tcontrollerOut\tsystemFeedback\thlNeuronAF\thlNeutronDL" << "\n";
    //myfile.close();
    //myfile.open ("../V-REP_DATA/log_file.dat", std::ios_base::app);

    if(ros::ok()) {
        if(simulation) {
//            rosHandle->synchronousSimulation(true);
//            rosHandle->rosSpinOnce();
        }
    }
}

bool neutronController::runController() {
    double amplitudeSensor, amplitudeController;
    double LPFSensorAmp, LPFControllerAmp;
    double error;
    double true_error;

    if(ros::ok()) {
        if(simulation)
            rosHandle->triggerSim();

        if(CPGmethod == 1) {
            // POST PROCESSING
            amplitudeSensor = sensorPostprocessor->calculateLPFAmplitude(rosHandle->jointPositions[BC4]);
            amplitudeController = controllerPostprocessor->calculateLPFAmplitude(positions.at(BC4));
            LPFSensorAmp = sensorAmpPostprocessor->calculateLPFSignal(amplitudeSensor);
            LPFControllerAmp = controllerAmpPostprocessor->calculateLPFSignal(amplitudeController);
            error = (LPFControllerAmp - errorMargin) - LPFSensorAmp;
            true_error = LPFControllerAmp - LPFSensorAmp;
        }

        // CALCULATE RBF CENTERS TODO IS IT EVEN NECESSARY TO CALCULATE CENTERS ONLINE? IF YES THEN FIND STABLE WAY
        CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));
        CPGPeriod = CPGPeriodPostprocessor->getPeriod();
        /*
        if(CPGPeriodPostprocessor->periodTrust){
            CPGAdaptive->calculateRBFCenters(CPGPeriodPostprocessor->getPeriod(),CPGPeriodPostprocessor->getSignalPeriod(0),CPGPeriodPostprocessor->getSignalPeriod(1));
        } */

        // New Phi for robot
        double newPhi = phiParam;

        if (CPGmethod == 0){
            /****************************************
            * VANILLA SO2 CPG
            * Open-Loop - Does not adapt in any way.
            ****************************************/
            CPGAdaptive->setPhii(newPhi);

        } else if(CPGmethod == 1) {
            /****************************************
            * DL CPG (Dual Learner)
            * Adapts the Phi to one where it can run.
            ****************************************/
            if(waiter >= 0) {
                error = 0;
                waiter--;
            } else {
                waiter = -10;
                learner->step(error, phiParam);
                CPGAdaptive->setPhii(learner->getControlOutput());
            }
        }

        vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

        // Plot's
        data.clear();
        // RED
        data.push_back(positions.at(BC0));//CPGAdaptive->getNetworkOutput()[0]);//controllerPostprocessor->periodViz);
        // GREEN
        data.push_back(positions.at(CF0));//CPGAdaptive->getNetworkOutput()[1]);//rosHandle->jointTorques[BC0]);
        // YELLOW
        data.push_back(positions.at(FT0));//CPGAdaptive->getCpgOutput(0));
        // BLUE
        data.push_back(0);//rosHandle->jointTorques[FT0]);
        // PINK
        data.push_back(0);//CPGAdaptive->getNetworkOutput()[2]);

        // STEP CPG AND EXECUTE TRIPOD CONTROLLER
        if(transientState) {
            // Fill up delay lines
            for (int k = 0; k <= tau+1; ++k) {
                CPGAdaptive->step();
                vector<double> RBF_output = CPGAdaptive->getNetworkOutput();
                for (int j = 0; j < 18; ++j)
                    phaseShift[j].Write(RBF_output[j]);
                for (auto &i : phaseShift)
                    i.Step();
            }
            transientState=false;
        } else {
            vector<double> RBF_output = CPGAdaptive->getNetworkOutput();
            CPGAdaptive->step();
            tripodGaitRBFN();
        }
    } else
    {
        cout << "Closing in the loop" << endl;
        return false;
    }

    rosHandle->rosSpinOnce();
    if(simulation) {
        if ((rosHandle->simState != 1 && rosHandle->simulationTime > 1) || rosHandle->terminateSimulation) {
            cout << "Closing and logging" << endl;
            cout << " " << endl;
            cout << " " << endl;

            myfile.close();
            rosHandle->synchronousSimulation(false);
            ros::shutdown();
            delete rosHandle;
            delete CPGAdaptive;
            delete learner;
            delete sensorPostprocessor;
            delete controllerPostprocessor;
            delete sensorAmpPostprocessor;
            delete controllerAmpPostprocessor;
            delete CPGPeriodPostprocessor;
            return false;
        } else {
            return !rosHandle->terminateSimulation;
        }
    } else
        return true;
}

void neutronController::tripodGaitRBFN() {

    // TODO IMPLEMENT SENSOR MODULATION FUNCTION
    // Should be a function F around RBF_output i.e. F(RBF_output) or an CPGAdaptive extra function
    vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

    for (int j = 0; j < 18; ++j)
        phaseShift[j].Write(RBF_output[j]);

    int end = RBF_output.size();

    // We force a tripod gait by delaying every second joint
    positions.at(BC0) = phaseShift[0].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(BC1) = phaseShift[1].Read(0);
    positions.at(BC2) = phaseShift[2].Read(0);
    positions.at(BC3) = phaseShift[3].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(BC4) = 0;
    positions.at(BC5) = 0;

    positions.at(CF0) = phaseShift[4].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(CF1) = phaseShift[5].Read(0);
    positions.at(CF2) = phaseShift[6].Read(0);
    positions.at(CF3) = phaseShift[7].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(CF4) = 0;
    positions.at(CF5) = 0;

    positions.at(FT0) = phaseShift[8].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(FT1) = phaseShift[9].Read(0);
    positions.at(FT2) = phaseShift[10].Read(0);
    positions.at(FT3) = phaseShift[11].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(FT4) = 0;
    positions.at(FT5) = 0;

    rosHandle->setLegMotorPosition(positions);

    if(simulation)
        rosHandle->plot(data);

    // Step delay lines
    for (auto &i : phaseShift)
        i.Step();
}

void neutronController::tripodGaitRangeOfMotion() {

    // CPG Amplitude
    float maxNetworkOut = 0.2;

    // BC
    float MotorOutputBC=rescale(maxNetworkOut,-maxNetworkOut,BC_pos,-BC_pos, CPGAdaptive->getCpgOutput(1));
    float MotorOutputBCNeg=rescale(maxNetworkOut,-maxNetworkOut,BC_pos,-BC_pos, -CPGAdaptive->getCpgOutput(1));

    // CF
    float MotorOutputCF=rescale(maxNetworkOut,-maxNetworkOut, CF_pos+0.7, CF_pos, CPGAdaptive->getCpgOutput(0));
    float MotorOutputCFNeg=rescale(maxNetworkOut,-maxNetworkOut, CF_pos+0.7, CF_pos, -CPGAdaptive->getCpgOutput(0));

    // FT
    float MotorOutputFT=rescale(maxNetworkOut,0, FT_pos+0.3, FT_pos, abs(CPGAdaptive->getCpgOutput(0)));
    float MotorOutputFTNeg=rescale(maxNetworkOut,0, FT_pos+0.3, FT_pos, abs(-CPGAdaptive->getCpgOutput(0)));

    // Threshold CF joint
    if (MotorOutputCF < CF_pos+CF_threshold)
        MotorOutputCF = CF_pos+CF_threshold;
    if (MotorOutputCFNeg < CF_pos+CF_threshold)
        MotorOutputCFNeg = CF_pos+CF_threshold;

    positions.at(BC0) = MotorOutputBC;
    positions.at(BC1) = MotorOutputBCNeg;
    positions.at(BC2) = MotorOutputBC;
    positions.at(BC3) = MotorOutputBCNeg;
    positions.at(BC4) = MotorOutputBC;
    positions.at(BC5) = MotorOutputBCNeg;

    positions.at(CF0) = MotorOutputCF;
    positions.at(CF1) = MotorOutputCFNeg;
    positions.at(CF2) = MotorOutputCF;
    positions.at(CF5) = MotorOutputCFNeg;
    positions.at(CF4) = MotorOutputCF;
    positions.at(CF3) = MotorOutputCFNeg;

    positions.at(FT0) = MotorOutputFT;
    positions.at(FT1) = MotorOutputFTNeg;
    positions.at(FT2) = MotorOutputFT;
    positions.at(FT5) = MotorOutputFTNeg;
    positions.at(FT4) = MotorOutputFT;
    positions.at(FT3) = MotorOutputFTNeg;

    rosHandle->setLegMotorPosition(positions);

    if(simulation)
        rosHandle->plot(data);
}

vector<float> neutronController::readParameterSet() {
    ifstream ifs("./RL_job.json");
    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document document;
    document.ParseStream(isw);

    assert(document.IsObject());
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    assert(document.HasMember("ParameterSet"));
    assert(document["ParameterSet"].IsArray());
    rapidjson::Value& paramset = document["ParameterSet"];

    std::string noise_name = "noise_" + std::to_string(rosHandle->rollout); //rollout not set in time!

    if(!document.HasMember(noise_name.c_str())){
        cout << "[ERROR] No noise member called: " << noise_name.c_str() << endl;
        cout << "[ERROR] Using noise member \"noise_0\" again" << endl;
        noise_name = "noise_0";
    }

    rapidjson::Value& noise = document[noise_name.c_str()];

    vector<float> weightsMatlab;
    for (rapidjson::SizeType i = 0; i < paramset.Size(); i++) // Uses SizeType instead of size_t
        weightsMatlab.push_back(paramset[i].GetDouble());// + noise[i].GetDouble());

    // Get encoding
    assert(document.HasMember("checked"));
    assert(document["checked"].IsString());
    rapidjson::Value& _encoding = document["checked"];
    encoding = _encoding.GetString();

    ifs.close();

    return weightsMatlab;
}

void neutronController::logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                                 double positionX, double bodyVel, double angularVelocity, double jointTorque,
                                 double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL){
    myfile << simtime <<"\t"<< CPGphi <<"\t"<< error <<"\t"<< maxVel <<"\t"<< maxForce <<"\t"<< positionX <<"\t" << bodyVel << "\t" << angularVelocity << "\t" << jointTorque << "\t" << controllerOut << "\t" << systemFeedback << "\t" << hlNeuronAF << "\t" << hlNeutronDL << "\n";
}

double neutronController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}
