//
// Created by mat on 8/2/17.
//

#ifndef ROS_HEXAPOD_CONTROLLER_realROSCLASS_H
#define ROS_HEXAPOD_CONTROLLER_realROSCLASS_H

#include <cstdio>
#include <cstdlib>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/ColorRGBA.h"
#include <std_msgs/Int32.h>
#include "sensor_msgs/Joy.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/Temperature.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include "neutronMotorDefinition.h"
#include <sensor_msgs/JointState.h>

class realRosClass {
private:
    // Subscribers
    ros::Subscriber motorFeedbackSub;
    ros::Subscriber joint_IDs;
    ros::Subscriber joint_positions;
    ros::Subscriber joint_torques;
    ros::Subscriber joint_velocities;
    ros::Subscriber joint_errorStates;
    ros::Subscriber joySub;
    ros::Subscriber joint_inputVoltage;
    ros::Subscriber imu_imu;
    ros::Subscriber imu_euler;
    ros::Subscriber imu_temp;

    // Publishers
    ros::Publisher jointControlPub;
    ros::Publisher blinkStickPub;


    // Private Global Variables
    ros::Rate* rate;

    // Private Methods
    void joint_IDs_CB(const std_msgs::Int32MultiArray&);
    void joint_positions_CB(const std_msgs::Float32MultiArray&);
    void joint_torques_CB(const std_msgs::Float32MultiArray&);
    void joint_velocities_CB(const std_msgs::Float32MultiArray&);
    void joint_errorStates_CB(const std_msgs::Float32MultiArray&);
    void joint_inputVoltage_CB(const std_msgs::Float32MultiArray&);
    void joy_CB(const sensor_msgs::Joy::ConstPtr& joy);
    void imu_imu_CB(const sensor_msgs::Imu::ConstPtr& imu);
    void imu_euler_CB(const geometry_msgs::Vector3::ConstPtr& euler);
    void imu_temp_CB(const sensor_msgs::Temperature::ConstPtr& temp);

public:
    // Public Methods
    realRosClass(int argc, char *argv[]);
    ~realRosClass();
    void setLegMotorPosition(std::vector<float> positions);
    void setLed(int R, int G, int B, int LED=0);

    void rosSpinOnce();

    // Place Holders Do nothing
    void plot(std::vector<float> data);
    void synchronousSimulation(unsigned char);
    void triggerSim();
    void triggerSimNoWait();
    int simState=0;
    bool shortMorf = true;

    // Public Global Variables
    std::vector<int> jointIDs               = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointPositions       = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointInputVoltage   = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointVelocities      = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointTorques         = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointErrorStates     = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> axes                 = {0,0,0,0,0,0,0,0};
    std::vector<int> buttons                = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool terminateSimulation    = false;
    int bodyFloorCollisions     = 0;
    int rollout                 = 0;
    float legCollisions         = 0.0;
    float avgPower              = 0.0;
    float avgBodyVel            = 0.0;
    float distance              = 0.0;
    float heightVariance        = 0.0;
    float headingDirection      = 0.0;
    float simulationTime        = 0.0;
};


#endif //ROS_HEXAPOD_CONTROLLER_realROSCLASS_H
