#include <utility>

//
// Created by mat on 12/21/18.
//

#include "rbfn.h"

rbfn::rbfn(int _numKernels, vector<float> _weights, string _encoding) {

    numKernels = _numKernels;
    beta = 0.04;
    weights = std::move(_weights);

    if (numKernels == 20){
        centers1 = pythoncenter1;
        centers2 = pythoncenter2;
    } else if (numKernels == 10){
        centers1 = pythoncenter1_10;
        centers2 = pythoncenter2_10;
    }

    encoding = _encoding;
}

vector<double> rbfn::step(double input1, double input2) {
    // We are using a Gaussian radial basis function
    // see https://www.mccormickml.com/2013/08/15/radial-basis-function-network-rbfn-tutorial/
    // and https://www.youtube.com/watch?v=1Cw45yNm6VA

    double kernelOutput = 0;
    double kernelOutput2 = 0;
    int numOfJoints = 12;
    int numOfLegs = 4;
    vector<double> rbfnOutput = vector<double> (numOfJoints+numOfLegs, 0);

    // TODO JUST ADD WEIGHTS (RBF) AND SENSOR WEIGHTS TOGETHER!!!! SEE JUPYTER SCRIPT
    // TODO START WITH MANUAL DESIGN - GET WEIGHTS FROM OPTIMIZED SIMULATION

    if (encoding == "direct") {
        for (int i = 0; i < numKernels; ++i) {
            kernelOutput = exp(-(pow(input1 - centers1[i], 2) + pow(input2 - centers2[i], 2)) / beta);

            for (int j = 0; j < numOfJoints; ++j) {
                rbfnOutput[j] += (weights[i + (j * numKernels)] * kernelOutput);
            }
        }

        // SET PHASES TO TRIPOD
        for (int k = 0; k < numOfLegs; ++k)
            rbfnOutput[numOfJoints+k] = 0.5;

    } else if (encoding == "sindirect") {
        for (int i = 0; i < numKernels; ++i) {
            kernelOutput = exp(-(pow(input1 - centers1[i], 2) + pow(input2 - centers2[i], 2)) / beta);

            int jj = 0;

            for (int j = 0; j < numOfJoints/2; j=j+2) {
                rbfnOutput[jj+0] += (weights[i + (j * numKernels)] * kernelOutput);
                rbfnOutput[jj+1] += (weights[i + ((j+1) * numKernels)] * kernelOutput);
                rbfnOutput[jj+2] += (weights[i + (j * numKernels)] * kernelOutput);
                rbfnOutput[jj+3] += (weights[i + ((j+1) * numKernels)] * kernelOutput);
                jj = jj+numOfLegs;
            }
        }

        // SET PHASES TO TRIPOD
        for (int k = 0; k < numOfLegs; ++k)
            rbfnOutput[numOfJoints+k] = 0.5;

    } else if (encoding == "indirect") {
        for (int i = 0; i < numKernels; ++i) {
            kernelOutput = exp(-(pow(input1 - centers1[i], 2) + pow(input2 - centers2[i], 2)) / beta);
            kernelOutput2 = exp(-(pow(-input1 - centers1[i], 2) + pow(-input2 - centers2[i], 2)) / beta);
            int jj = 0;

            for (int j = 0; j < 3; ++j) {
                rbfnOutput[jj+0] += (weights[i + (j * numKernels)] * kernelOutput);
                rbfnOutput[jj+1] += (weights[i + (j * numKernels)] * kernelOutput2);
                rbfnOutput[jj+2] += (weights[i + (j * numKernels)] * kernelOutput2);

                rbfnOutput[jj+3] += (weights[i + (j * numKernels)] * kernelOutput);
                jj = jj+numOfLegs;
            }
        }

        // SET PHASES TO TRIPOD
        for (int k = 0; k < numOfLegs; ++k)
            rbfnOutput[numOfJoints+k] = 0.5;

    } else
        cout << "[ ERROR] UNKNOWN ENCODING!" << endl;

    return rbfnOutput;
}

void rbfn::setBeta(double _beta) {
    beta = _beta;
}

void rbfn::setWeights(vector<float> _weights) {
    weights = _weights;
}

int rbfn::getNumKernels() {
    return numKernels;
}

void rbfn::setCenters(vector<float> _centers1, vector<float> _centers2) {
    centers1 = _centers1;
    centers2 = _centers2;
}

double rbfn::getBeta() {
    return beta;
}

vector<float> rbfn::getWeights() {
    return weights;
}

vector<float> rbfn::setCenters(int center) {
    if (center == 1)
        return centers1;
    else
        return centers2;
}

void rbfn::calculateCenters(int period, vector<float> signal1, vector<float> signal2) {
    vector<float> _centers = linspace(1, period, numKernels);
    centers1 = _centers;
    centers2 = _centers;

    for (int i = 0; i < numKernels; ++i) {
        centers1[i] = signal1[_centers[i]];
        centers2[i] = signal2[_centers[i]];
    }
}

template<typename T>
std::vector<float> rbfn::linspace(T start_in, T end_in, int num_in)
{

    std::vector<float> linspaced;

    double start = static_cast<double>(start_in);
    double end = static_cast<double>(end_in);
    double num = static_cast<double>(num_in);

    if (num == 0) { return linspaced; }
    if (num == 1)
    {
        linspaced.push_back(start);
        return linspaced;
    }

    double delta = (end - start) / (num - 1);

    for(int i=0; i < num-1; ++i)
    {
        linspaced.push_back(start + delta * i);
    }
    linspaced.push_back(end); // I want to ensure that start and end
    // are exactly the same as the input
    return linspaced;
}
