--[[
Callback function for receiving motor positions over ROS
--]]

-- Constants
math.randomseed(os.time())

local function roundToNthDecimal(num, n)
    local mult = 10^(n or 0)
    return math.floor(num * mult + 0.5) / mult
end

function mean( t )
    local sum = 0
    local count= 0
    for k,v in pairs(t) do
        if type(v) == 'number' then
            sum = sum + v
            count = count + 1
        end
    end
    return (sum / count)
end

function standardDeviation( t )
    local m
    local vm
    local sum = 0
    local count = 0
    local result
    m = mean( t )
    for k,v in pairs(t) do
        if type(v) == 'number' then
            vm = v - m
            sum = sum + (vm * vm)
            count = count + 1
        end
    end
    result = math.sqrt(sum / (count-1))
    return result
end

function gaussian (mean, variance)
    return  math.sqrt(-2 * variance * math.log(math.random())) * math.cos(2 * math.pi * math.random()) + mean
end

function simGetJointVelocity (jointHandle)
    res,velocity=simGetObjectFloatParameter(jointHandle,2012)
    return  velocity
end

function absmean (prev_avg, x, n)
    return ((prev_avg * n + math.abs(x)) / (n + 1));
end

function absmean_arr (numlist)
    if type(numlist) ~= 'table' then
        print("Error in absmean_arr")
        return numlist
    end
    num = 0
    table.foreach(numlist,function(i,v) num=num+math.abs(v) end)
    return num / #numlist
end

function slip_detector( object_handle, vel_threshold )
    index=0
    objectsInContact,contactPt,forceDirectionAndAmplitude=sim.getContactInfo(sim.handle_all,object_handle,index)

    linearVelocity, angularVelocity = sim.getVelocity(object_handle)
    absLinearVelocity = math.sqrt((linearVelocity[1]*linearVelocity[1]) + (linearVelocity[2]*linearVelocity[2]))

    if objectsInContact then
        if absLinearVelocity > vel_threshold then return 1 end
        return 0
    else return 0
    end
end

function setMotorPositions_cb(msg)
    data = msg.data

    sim.setJointTargetPosition(TC_motor0,data[2])
    sim.setJointTargetPosition(CF_motor0,data[4])
    sim.setJointTargetPosition(FT_motor0,data[6])

    sim.setJointTargetPosition(TC_motor1,data[8])
    sim.setJointTargetPosition(CF_motor1,data[10])
    sim.setJointTargetPosition(FT_motor1,data[12])

    sim.setJointTargetPosition(TC_motor2,data[14])
    sim.setJointTargetPosition(CF_motor2,data[16])
    sim.setJointTargetPosition(FT_motor2,data[18])

    sim.setJointTargetPosition(TC_motor3,data[20])
    sim.setJointTargetPosition(CF_motor3,data[22])
    sim.setJointTargetPosition(FT_motor3,data[24])
end

--[[
Initialization: Called once at the start of a simulation
--]]
if (sim_call_type==sim.childscriptcall_initialization) then

    simulationID=sim.getIntegerSignal("simulationID")

--     print("************")
--     print("LAIKAGO: "..simulationID)
--     print("************")

    stepCounter     = 0
    mean_vel        = 0
    mean_jtor       = 0
    mean_jvel       = 0
    mean_jpower     = 0
    mean_pan        = 0
    mean_tilt       = 0
    mean_roll       = 0
    mean_height     = 0
    mean_slip       = 0
    offset_pan      = 0
    update_count    = 0
    bodyfloor_collisions = 0
    leg_collisions = 0
    collisionLast1 = false
    collisionLast2 = false
    collisionLast3 = false
    collisionLast4 = false
    collisionLastBF = false
    boolswitch     = true
    height_arr = {}
    oriX_arr = {}
    oriY_arr = {}
    orientation_arr = {}
    circlebreak = false;
    testParameters = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    largest_dist = 0
    collisions_max = 0

    -- Create all handles
    robotHandle=sim.getObjectAssociatedWithScript(sim.handle_self)

    TC_motor0=sim.getObjectHandle("TC0")    -- Handle of the TC motor
    TC_motor1=sim.getObjectHandle("TC1")    -- Handle of the TC motor
    TC_motor2=sim.getObjectHandle("TC2")    -- Handle of the TC motor
    TC_motor3=sim.getObjectHandle("TC3")    -- Handle of the TC motor

    CF_motor0=sim.getObjectHandle("CF0")    -- Handle of the CF motor
    CF_motor1=sim.getObjectHandle("CF1")    -- Handle of the CF motor
    CF_motor2=sim.getObjectHandle("CF2")    -- Handle of the CF motor
    CF_motor3=sim.getObjectHandle("CF3")    -- Handle of the CF motor

    FT_motor0=sim.getObjectHandle("FT0")    -- Handle of the FT motor
    FT_motor1=sim.getObjectHandle("FT1")    -- Handle of the FT motor
    FT_motor2=sim.getObjectHandle("FT2")    -- Handle of the FT motor
    FT_motor3=sim.getObjectHandle("FT3")    -- Handle of the FT motor

    tipHandles = { sim.getObjectHandle("tip_dyn0"),
                   sim.getObjectHandle("tip_dyn1"),
                   sim.getObjectHandle("tip_dyn2"),
                   sim.getObjectHandle("tip_dyn3")}

    IMU=sim.getObjectHandle("Imu")
    distHandle_leg01=sim.getDistanceHandle("leg01")
    distHandle_leg12=sim.getDistanceHandle("leg23")


    distHandle_BF   =sim.getDistanceHandle("bodyfloor")
    morfHexapod=sim.getObjectHandle("laikago")

    previousTime=0

    -- Check if the required ROS plugin is loaded

    moduleName=0
    moduleVersion=0
    index=0
    pluginNotFound=true
    while moduleName do
        moduleName,moduleVersion=sim.getModuleName(index)

        print(moduleName)

        if (moduleName=='ROSInterface') then
            pluginNotFound=false
        end
        index=index+1
    end




    if (pluginNotFound) then
        sim.displayDialog('Error','The RosInterface was not found.',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
        printToConsole('[ERROR] The RosInterface was not found.')
    end

    -- If found then start the subscribers and publishers
    if (not pluginNotFound) then
        -- Create the subscribers
        MotorSub=simROS.subscribe('/'..'morf_sim'..simulationID..'/multi_joint_command','std_msgs/Float32MultiArray','setMotorPositions_cb')
        -- Create the publishers
        jointPositionsPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_positions','std_msgs/Float32MultiArray')
        jointTorquesPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_torques','std_msgs/Float32MultiArray')
        jointVelocitiesPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_velocities','std_msgs/Float32MultiArray')
        imuEulerPub=simROS.advertise('/morf_sim'..simulationID..'/euler','geometry_msgs/Vector3')
        testParametersPub=simROS.advertise('/'..'morf_sim'..simulationID..'/testParameters','std_msgs/Float32MultiArray')
    end

    simROS.publish(testParametersPub,{data=testParameters})

    -- WAIT FOR ROS TO START FULLY TO LAUNCH
    printToConsole('[ INFO] Initialized simulation')
end

--[[
Actuation: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_actuation) then
    -- Publish
    -- sim.setFloatSignal("mySimulationTime", sim.getSimulationTime())
end

--[[
Sensing: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_sensing) then
    -- Publish
    position_array  ={  simGetJointPosition(TC_motor0),simGetJointPosition(CF_motor0),simGetJointPosition(FT_motor0),
                        simGetJointPosition(TC_motor1),simGetJointPosition(CF_motor1),simGetJointPosition(FT_motor1),
                        simGetJointPosition(TC_motor2),simGetJointPosition(CF_motor2),simGetJointPosition(FT_motor2),
                        simGetJointPosition(TC_motor3),simGetJointPosition(CF_motor3),simGetJointPosition(FT_motor3),
                        0, 0, 0,
                        0, 0, 0  }

    velocity_array  ={  simGetJointVelocity(TC_motor0),simGetJointVelocity(CF_motor0),simGetJointVelocity(FT_motor0),
                        simGetJointVelocity(TC_motor1),simGetJointVelocity(CF_motor1),simGetJointVelocity(FT_motor1),
                        simGetJointVelocity(TC_motor2),simGetJointVelocity(CF_motor2),simGetJointVelocity(FT_motor2),
                        simGetJointVelocity(TC_motor3),simGetJointVelocity(CF_motor3),simGetJointVelocity(FT_motor3),
                        0, 0, 0,
                        0, 0, 0  }

    torque_array    ={  simGetJointForce(TC_motor0),simGetJointForce(CF_motor0),simGetJointForce(FT_motor0),
                        simGetJointForce(TC_motor1),simGetJointForce(CF_motor1),simGetJointForce(FT_motor1),
                        simGetJointForce(TC_motor2),simGetJointForce(CF_motor2),simGetJointForce(FT_motor2),
                        simGetJointForce(TC_motor3),simGetJointForce(CF_motor3),simGetJointForce(FT_motor3),
                        0, 0, 0,
                        0, 0, 0  }


    -- **************** --
    -- Fitness feedback --
    -- **************** --
    linearVelocity, aVelocity=sim.getObjectVelocity(IMU) -- m/s
    objectPosition = sim.getObjectPosition(IMU,-1)
    objectOrientation = sim.getObjectOrientation(IMU,-1)

    -- Mean velocity of robot
    mean_vel = absmean(mean_vel, linearVelocity[1], update_count)

    -- Mean power
    mean_jtor   = absmean(mean_jtor, absmean_arr(torque_array), update_count)
    mean_jvel   = absmean(mean_jvel, absmean_arr(velocity_array), update_count)
    mean_jpower = absmean(mean_jpower, mean_jtor * mean_jvel, update_count)

    -- Orientation / Stability
    mean_roll   = absmean(mean_roll, objectOrientation[1], update_count)
    mean_tilt   = absmean(mean_tilt, objectOrientation[2], update_count)
    mean_pan    = absmean(mean_pan, objectOrientation[3]-offset_pan, update_count)

    -- Position
    table.insert(height_arr, objectPosition[3])

    -- Distance between legs
    max_detect_interleg     = 0.1
    max_detect_intraleg     = 0.005
    max_detect_bodyfloor    = 0.03
    collisionState = {max_detect_interleg,max_detect_interleg,max_detect_interleg,max_detect_interleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_bodyfloor}

    -- inter leg.
    result, distance_leg01=sim.handleDistance(distHandle_leg01) -- m
    if distance_leg01 == nil then collisionState[1]=max_detect_interleg else collisionState[1] = distance_leg01 end
    result, distance_leg12=sim.handleDistance(distHandle_leg12) -- m
    if distance_leg12 == nil then collisionState[2]=max_detect_interleg else collisionState[2] = distance_leg12 end


    -- body floor.
    result, distance_BF=sim.handleDistance(distHandle_BF) -- m
    if distance_BF == nil then collisionState[11]=max_detect_bodyfloor else collisionState[8] = distance_BF end

    collisionState[1] = 1 - (collisionState[1])   / ( max_detect_interleg ) -- leg01
    collisionState[2] = 1 - (collisionState[2])   / ( max_detect_interleg ) -- leg12
    collisionState[3] = 1 - (collisionState[3])   / ( max_detect_interleg ) -- leg34
    collisionState[4] = 1 - (collisionState[4])   / ( max_detect_interleg ) -- leg45
    collisionState[5] = 1 - (collisionState[5])   / ( max_detect_intraleg ) -- leg0s
    collisionState[6] = 1 - (collisionState[6])   / ( max_detect_intraleg ) -- leg1s
    collisionState[7] = 1 - (collisionState[7])   / ( max_detect_intraleg ) -- leg2s
    collisionState[8] = 1 - (collisionState[8])   / ( max_detect_intraleg ) -- leg3s
    collisionState[9] = 1 - (collisionState[9])   / ( max_detect_intraleg ) -- leg4s
    collisionState[10] = 1 - (collisionState[10]) / ( max_detect_intraleg ) -- leg5s
    collisionState[11] = 1 - (collisionState[11]) / ( max_detect_bodyfloor) -- bodyfloor

    --print(collisionState)
    max_dist = math.max(unpack(collisionState))
    if max_dist > 1 then print("[ ERROR]: Please set max dist correctly") end
    if max_dist > collisions_max then collisions_max = max_dist end

    positionRobot=sim.getObjectPosition(morfHexapod, -1)
    distance = -positionRobot[2] -- use negative world y axis

    slip_results = {}
    for i = 1, table.getn(tipHandles), 1 do
        table.insert(slip_results, slip_detector( tipHandles[i], 0.025))
    end

    max_slip = math.max(unpack(slip_results))

    if max_slip ~= -1 then
        mean_slip    = absmean(mean_slip, max_slip, update_count)
    end

    -- Remove transient period
    if simGetSimulationTime() < 1.3 then
        mean_slip=0
        mean_tilt=0
        mean_roll=0
        mean_pan =0
        collisions_max=0
        mean_height=0
        offset_pan = objectOrientation[3]
        height_arr = {0}
    end

    if simGetSimulationTime() < 1 and boolswitch then
        boolswitch = false
        -- Release the robot
        simSetObjectInt32Parameter(morfHexapod, sim_shapeintparam_static, 0)
    end

    testParameters[2]  = mean_slip -- x orientation
    testParameters[3]  = mean_tilt -- x orientation
    testParameters[4]  = mean_roll -- y orientation
    testParameters[5]  = mean_pan -- Heading = z orientation
    testParameters[6]  = collisions_max
    testParameters[7]  = 0.0 -- was body floor collisions (now included in collisions_max)
    testParameters[8]  = mean_jpower
    testParameters[9]  = mean_vel
    testParameters[10] = distance
    testParameters[11] = standardDeviation(height_arr)

    update_count = update_count + 1

    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(jointVelocitiesPub,{data=velocity_array})
    simROS.publish(jointTorquesPub,{data=torque_array})
    simROS.publish(testParametersPub,{data=testParameters})
end

--[[
Clean up: This part will be executed one time just before a simulation ends
--]]
if (sim_call_type==sim.childscriptcall_cleanup) then

    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(jointVelocitiesPub,{data=velocity_array})
    simROS.publish(jointTorquesPub,{data=torque_array})
    simROS.publish(testParametersPub,{data=testParameters})

    print("+====Objectives====+")
    print("Avg tilt:\t"    .. roundToNthDecimal(mean_tilt,4))
    print("Avg roll:\t"    .. roundToNthDecimal(mean_roll,4))
    print("Avg heading:\t" .. roundToNthDecimal(mean_pan,4))
    print("Avg Height:\t"  .. roundToNthDecimal(mean_height, 4))
    print("Avg power:\t"   .. roundToNthDecimal(mean_jpower,4))
    print("Robot Coll.:\t" .. roundToNthDecimal(collisions_max,5))
    print("Slipping:\t"     .. roundToNthDecimal(mean_slip, 4))
    print("Distance:\t"     .. roundToNthDecimal(distance,4))
    print("+================+")

    -- Set object static Controller
    simSetObjectInt32Parameter(morfHexapod, sim_shapeintparam_static, 1)

    -- Terminate remaining local notes
    simROS.shutdownSubscriber(MotorSub)
    simROS.shutdownPublisher(jointTorquesPub)
    simROS.shutdownPublisher(jointVelocitiesPub)
    simROS.shutdownPublisher(jointPositionsPub)
    simROS.shutdownPublisher(testParametersPub)
    simROS.shutdownPublisher(imuEulerPub)

    printToConsole('[ INFO] Lua child script stopped')
end
